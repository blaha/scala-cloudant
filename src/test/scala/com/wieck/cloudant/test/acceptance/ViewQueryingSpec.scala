package com.wieck.cloudant.test.acceptance

import com.wieck.cloudant.design._
import scala.concurrent.ExecutionContext.Implicits.global
import com.wieck.cloudant._
import com.wieck.cloudant.test.support._

class ViewQueryingSpec extends AcceptanceSpec {

  import Implicits._

  feature("View querying") {

    object AnimalsView extends ViewDefinition {

      val name = "animals"

      val mapReduce = MapReduce(
        """
          |function(doc) {
          |  emit([doc.diet, doc.name], doc.diet);
          |}
        """.stripMargin
      )

      type Key = (String, String)

      type Value = String

      def query(database: Database, designDocumentId: String) =
        View[Key, Value](database, designDocumentId, name).query

    }

    val designDocument = DesignDocument(Seq(AnimalsView))

    def bootstrap =
      await(
        database.bulkPut(
          List(Animals.aardvark, Animals.badger, Animals.elephant, Animals.kookaburra) map { animal =>
            NewDocument(animal.name, animal)
          },
          quorum = Some(3)
        ) flatMap (_ => database.createDesignDocument("animals", designDocument))
      )

    scenario("Querying a view with values and documents") {
      bootstrap

      val results = await(AnimalsView.query(database, "animals").includeDocs.execute)

      results.keys.map(_._2) should equal(List("kookaburra", "elephant", "aardvark", "badger"))
      results.values should equal(List("carnivore", "herbivore", "omnivore", "omnivore"))
      results.docsAs[Animal] should equal(List(Animals.kookaburra, Animals.elephant, Animals.aardvark, Animals.badger))
    }

    scenario("Querying a view by key") {
      bootstrap

      val results = await(AnimalsView.query(database, "animals").key(("omnivore", "aardvark")).execute)
      results.keys should equal(List((("omnivore", "aardvark"))))
    }

    scenario("Querying a view by start and end keys") {
      bootstrap

      val results = await(AnimalsView.query(database, "animals").startKey(("omnivore", KeyBounds.Min)).
        endKey(("omnivore", KeyBounds.Max)).execute)
      results.keys.map(_._2) should equal(List("aardvark", "badger"))
    }

    scenario("Querying a view with pagination") {
      bootstrap

      val results1 = await(AnimalsView.query(database, "animals").page.pageSize(2).execute)
      results1.keys.map(_._2) should equal(List("kookaburra", "elephant"))

      val results2 = await(AnimalsView.query(database, "animals").page.pageSize(2).key(results1.nextKey.get).execute)
      results2.keys.map(_._2) should equal(List("aardvark", "badger"))
    }

  }

}

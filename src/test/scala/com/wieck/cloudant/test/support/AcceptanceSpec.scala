package com.wieck.cloudant.test.support

import com.wieck.cloudant._
import com.typesafe.config.ConfigFactory
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures

trait AcceptanceSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfter with ShouldMatchers with SpecHelpers
  with CloudantProtocol with ScalaFutures {

  val config = ConfigFactory.load()
  val hostname = config.getString("hostname")
  val username = config.getString("username")
  val password = config.getString("password")

  val client = Client(hostname, username, password)
  var database: Database = _

  before {
    database = await(client.createDatabase(tempDatabaseName))
  }

  after {
    await(database.delete())
    database = null
  }

}

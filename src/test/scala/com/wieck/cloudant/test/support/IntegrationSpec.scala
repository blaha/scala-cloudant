package com.wieck.cloudant.test.support

import scala.util.Try
import com.wieck.cloudant._
import com.typesafe.config.ConfigFactory
import org.scalatest._

trait IntegrationSupport extends SpecHelpers {
  val config = ConfigFactory.load()
  val hostname = config.getString("hostname")
  val username = config.getString("username")
  val password = config.getString("password")

  val client = Client(hostname, username, password)

  def withDatabase(f: Database => Unit) {
    val database = await(client.createDatabase(tempDatabaseName))
    val result = Try(f(database))
    Try { await(database.delete()) }
    result.get
  }

  def deleteDatabaseAfter(databaseName: String)(f: String => Unit) {
    val result = Try(f(databaseName))
    Try { await(client.deleteDatabase(databaseName)) }
    result.get
  }
}


trait IntegrationSpec extends FunSpec
  with IntegrationSupport
  with ShouldMatchers
  with CloudantProtocol

trait WordIntegrationSpec extends WordSpec
  with IntegrationSupport
  with ShouldMatchers
  with CloudantProtocol

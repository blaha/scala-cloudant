package com.wieck.cloudant.test.integration

import com.wieck.cloudant.design._
import scala.concurrent.ExecutionContext.Implicits.global
import spray.json._
import com.wieck.cloudant._
import com.wieck.cloudant.test.support._

class QueryViewSpec extends IntegrationSpec {

  import Implicits._

  describe("Querying a view") {

    val designDocument = DesignDocument(
      "myView" -> MapReduce(
        """
          |function(doc) {
          |  emit(doc._id, doc.name);
          |}
        """.stripMargin
      )
    )

    def bootstrap(database: Database) =
      await {
        for {
          _ <- database.createDesignDocument("myDesign", designDocument)
          _ <- database.putDocument(NewDocument("aardvark", Animals.aardvark))
          _ <- database.ensureFullCommit
        } yield ()
      }

    describe("when the design document exists") {

      it("succeeds") {
        withDatabase { database =>
          bootstrap(database)
          await(database.queryView[String, Null]("myDesign", "myView"))
        }
      }

      it("returns the right keys") {
        withDatabase { database =>
          bootstrap(database)
          val results = await(database.queryView[String, Null]("myDesign", "myView"))
          results.keys should equal(List("aardvark"))
        }
      }

      it("returns the right values") {
        withDatabase { database =>
          bootstrap(database)
          val results = await(database.queryView[String, String]("myDesign", "myView"))
          results.values should equal(List("aardvark"))
        }
      }

    }

    describe("options") {

      describe("without includeDocs") {

        it("returns null docs") {
          withDatabase { database =>
            bootstrap(database)
            val results = await(database.queryView[String, String]("myDesign", "myView"))
            results.docs should equal(List(None))
          }
        }

      }

      describe("with includeDocs") {

        it("returns the docs as Json objects") {
          withDatabase { database =>
            bootstrap(database)
            val view = View[String, String](database, "myDesign", "myView")

            val results = await(view.query.includeDocs.execute)
            results.docs.size should equal(1)
            results.docs(0).get.fields("name") should equal(JsString("aardvark"))
          }
        }

      }

    }

  }

}

package com.wieck.cloudant.test.integration

import com.wieck.cloudant.exceptions._
import com.wieck.cloudant.test.support.IntegrationSpec

class CreateDatabaseSpec extends IntegrationSpec {

  describe("Creating a database") {

    describe("when the database does not already exist") {

      it("succeeds") {
        deleteDatabaseAfter(tempDatabaseName) { name =>
          await(client.createDatabase(name))
        }
      }

    }

    describe("when the database already exists") {

      it("fails with a DatabaseAlreadyExistsException") {
        deleteDatabaseAfter(tempDatabaseName) { name =>
          await(client.createDatabase(name))
          val result = client.createDatabase(name)
          intercept[DatabaseAlreadyExistsException] {
            await(result)
          }
        }
      }

    }

  }

}

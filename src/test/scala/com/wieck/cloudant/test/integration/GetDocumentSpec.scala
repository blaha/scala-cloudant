package com.wieck.cloudant.test.integration

import com.wieck.cloudant._
import com.wieck.cloudant.exceptions._
import com.wieck.cloudant.test.support._

class GetDocumentSpec extends IntegrationSpec {

  describe("Getting a document") {

    describe("when the document exists") {

      it("succeeds") {
        withDatabase { database =>
          await(database.putDocument(NewDocument("aardvark", Animals.aardvark)))
          await(database.getDocument[Animal]("aardvark"))
        }
      }

    }

    describe("when the document does not exist") {

      it("raises a DocumentNotFoundException") {
        withDatabase { database =>
          intercept[DocumentNotFoundException] {
            await(database.getDocument[Animal]("foo"))
          }
        }
      }

    }

  }

}

package com.wieck.cloudant.test.integration

import com.wieck.cloudant.exceptions._
import com.wieck.cloudant.test.support.IntegrationSpec

class DeleteDatabaseSpec extends IntegrationSpec {

  describe("Deleting a database") {

    describe("when the database does not already exist") {

      it("fails with a DatabaseNotFoundException") {
        intercept[DatabaseNotFoundException] {
          await(client.deleteDatabase(tempDatabaseName))
        }
      }

    }

    describe("when the database already exists") {

      it("succeeds") {
        val databaseName = tempDatabaseName
        await(client.createDatabase(databaseName))
        await(client.deleteDatabase(databaseName))
      }

    }

  }

}

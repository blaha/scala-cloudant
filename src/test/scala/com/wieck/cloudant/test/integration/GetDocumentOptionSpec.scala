package com.wieck.cloudant.test.integration

import com.wieck.cloudant._
import com.wieck.cloudant.exceptions._
import com.wieck.cloudant.test.support._

class GetDocumentOptionSpec extends IntegrationSpec {

  describe("Getting a document Option") {

    describe("when the document exists") {

      it("returns Some document") {
        withDatabase { database =>
          await(database.putDocument(NewDocument("aardvark", Animals.aardvark)))
          await(database.getDocumentOption[Animal]("aardvark")) should be ('defined)
        }
      }

    }

    describe("when the document does not exist") {

      it("returns None") {
        withDatabase { database =>
          await(database.getDocumentOption[Animal]("foo")) should be (None)
        }
      }

    }

  }

}

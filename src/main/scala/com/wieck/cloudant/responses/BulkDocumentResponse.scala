package com.wieck.cloudant.responses

case class BulkDocumentResponse(val id: String, val rev: String)
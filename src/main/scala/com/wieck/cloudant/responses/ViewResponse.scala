package com.wieck.cloudant.responses

import spray.json._

import spray.json.JsonFormat

case class ViewRow[K, V](key: K, value: V, doc: Option[JsObject]) {

  def docAs[A: JsonFormat] = doc.map(_.convertTo[A])

}

case class ViewResponse[K, V](total_rows: Int, offset: Int, rows: List[ViewRow[K, V]]) {

  def keys = rows.map(_.key)

  def values = rows.map(_.value)

  def docs = rows.map(_.doc)

  def docsAs[A: JsonFormat] = rows.flatMap(_.docAs[A])

  def reverse: ViewResponse[K, V] = ViewResponse[K, V](total_rows, offset, rows.reverse)

}

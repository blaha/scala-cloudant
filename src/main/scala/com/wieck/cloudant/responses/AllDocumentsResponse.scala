package com.wieck.cloudant
package responses

import spray.json._

case class AllDocumentsValue(rev: String)

case class AllDocumentsRow(id: String, key: JsValue, value: AllDocumentsValue) {

  def rev: String = value.rev

}

case class AllDocumentsResponse(total_rows: Int, offset: Int, rows: Seq[AllDocumentsRow])

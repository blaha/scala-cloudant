package com.wieck.cloudant

case class DocumentResponse(ok: Option[Boolean], id: String, rev: String)

package com.wieck.cloudant.responses

import com.wieck.cloudant.CloudantProtocol
import com.wieck.cloudant.design.IndexSearch
import scala.concurrent.{blocking, Await, Future, ExecutionContext}
import scala.concurrent.duration._
import spray.json.{JsValue, JsonFormat}


case class SearchRow[K, V](id: K, order: List[JsValue], fields: V, doc: Option[JsValue]) {
  def docAs[A: JsonFormat] = doc map(_.convertTo[A])
}


case class SearchResponse[K, V](total_rows: Int, bookmark: String, rows: List[SearchRow[K, V]]) {
  def docs[A: JsonFormat] = rows flatMap(_.docAs[A])
}


/**
 * Stream the search response rows for easier processing.
 *
 * This only works if all of the values for the target object are stored in the index
 * (if you're using a JSON-class mapping), or if you process the results as `JsObject`
 * or some such.
 *
 * Also, the potential exists for blocking: although the server fetch is done in a background
 * thread, if the head of the stream isn't available when requested, then the call will block
 * until fulfilled or the timeout is reached.
 *
 * @param query   The search query.
 * @param timeout The timeout for fetching the head of the stream (note that this is ''not''
 *                a timeout used for retrieving results from the server, but rather the timeout
 *                for that to complete ''after'' the head is requested).
 */
class ResponseStreamer[K, V: JsonFormat](query: IndexSearch[K], timeout: Duration = 10 seconds)(implicit ec: ExecutionContext) {
  @volatile private[this] var totalRowCount = -1

  /**
   * Provides the total row count, but only after the first element of the stream
   * has been returned to the caller; before that it will return `-1`.
   */
  def totalRows: Int = totalRowCount


  /**
   * Returns the search rows as a stream. The query is executed and the response fetched
   * using a `Future`, but if the future hasn't completed by the time you request the first
   * element, it will block until the provided timeout occurs (defaulting to 10 seconds).
   */
  def stream: Future[Stream[SearchRow[K,V]]] = {
    def next(index: Int, processed: Int, futureResponse: Future[SearchResponse[K,V]]): Stream[SearchRow[K,V]] = blocking {
      val response = Await.result(futureResponse, timeout)

      // we now have a value for this, so make it available
      totalRowCount = response.total_rows

      if (processed == response.total_rows || index >= response.rows.size) Stream.empty
      else {
        val result = response.rows(index)
        val tail = if (index + 1 == response.rows.size) next(0, processed + 1, fetch(Some(response.bookmark)))
                    else next(index + 1, processed + 1, futureResponse)
        result #:: tail
      }
    }

    Future( next(0, 0, fetch(None)) )
  }


  private def fetch(bookmark: Option[String]): Future[SearchResponse[K,V]] = {
    query.bookmark(bookmark).execute[V]
  }
}

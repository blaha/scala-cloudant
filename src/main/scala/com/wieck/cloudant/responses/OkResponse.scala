package com.wieck.cloudant.responses

case class OkResponse(ok: Boolean)

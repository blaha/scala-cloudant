package com.wieck.cloudant.query

import spray.json._

case class PaginationOptions[K: JsonFormat](direction: PaginationDirection.PaginationDirection, pageSize: Int,
  key: Option[K]) {

  def asQueryParams: Map[String, String] = Map(
    "descending" -> Some((direction == PaginationDirection.Reverse)),
    "limit" -> Some((pageSize + 2)),
    "startkey" -> key.map(_.toJson)
  ) collect { case(a, Some(b)) => (a, b.toString()) }

}
package com.wieck.cloudant.query

import spray.json.JsonFormat
import PaginationDirection._
import com.wieck.cloudant.responses.{ViewRow, ViewResponse}

case class PaginatedViewResponse[K: JsonFormat, V: JsonFormat](viewResponse: ViewResponse[K, V], size: Int,
  direction: PaginationDirection, key: Option[K]) {

  val orderedViewResponse = direction match {
    case Forward => viewResponse
    case Reverse => viewResponse.reverse
  }

  def mapKeys[T: JsonFormat](f: K => T): PaginatedViewResponse[T, V] = {
    copy[T, V](
      viewResponse = viewResponse.copy(
        rows = rows.map(
          row => row.copy[T, V](key = f(row.key))
        )
      ),
      key = key.map(f)
    )
  }

  def docs[A: JsonFormat]: List[A] = {
    key match {
      case Some(k) => {
        direction match {
          case Forward => orderedViewResponse.docsAs[A].drop(1).take(size)
          case Reverse => orderedViewResponse.docsAs[A].reverse.drop(1).take(size).reverse
        }
      }
      case None =>
        direction match {
          case Forward => orderedViewResponse.docsAs[A].take(size)
          case Reverse => orderedViewResponse.docsAs[A].reverse.take(size).reverse
        }
    }
  }

  def rows: List[ViewRow[K, V]] = {
    key match {
      case Some(k) => {
        direction match {
          case Forward => orderedViewResponse.rows.drop(1).take(size)
          case Reverse => orderedViewResponse.rows.reverse.drop(1).reverse.take(size)
        }
      }
      case None => orderedViewResponse.rows.take(size)
    }
  }

  def keys: List[K] = {
    key match {
      case Some(k) => {
        direction match {
          case Forward => orderedViewResponse.keys.drop(1).take(size)
          case Reverse => orderedViewResponse.keys.reverse.drop(1).reverse.take(size)
        }
      }
      case None => orderedViewResponse.keys.take(size)
    }
  }

  def values: List[V] = {
    key match {
      case Some(k) => {
        direction match {
          case Forward => orderedViewResponse.values.drop(1).reverse.take(size)
          case Reverse => orderedViewResponse.values.reverse.drop(1).reverse.take(size)
        }
      }
      case None => orderedViewResponse.values.take(size)
    }
  }

  def nextKey: Option[K] = {
    direction match {
      case Forward => forwardKey
      case Reverse => reverseKey
    }
  }

  def previousKey: Option[K] = {
    direction match {
      case Forward => reverseKey
      case Reverse => forwardKey
    }
  }

  private def forwardKey: Option[K] = {
    key match {
      case Some(_) =>
        if (orderedViewResponse.keys.size >= size + 2) {
          direction match {
            case Forward => Some(orderedViewResponse.keys.drop(size).head)
            case Reverse => Some(orderedViewResponse.keys.reverse.drop(size).head)
          }
        } else {
          None
        }
      case None =>
        if (orderedViewResponse.keys.size >= size + 1) {
          direction match {
            case Forward => Some(orderedViewResponse.keys.drop(size - 1).head)
            case Reverse => Some(orderedViewResponse.keys.reverse.drop(size - 1).head)
          }
        } else {
          None
        }
    }

  }

  private def reverseKey: Option[K] = {
    key match {
      case Some(_) =>
        direction match {
          case Forward => Some(orderedViewResponse.keys.drop(1).head)
          case Reverse => Some(orderedViewResponse.keys.reverse.drop(1).head)
        }
      case None => None
    }
  }

}

package com.wieck.cloudant.query

import spray.json._
import com.wieck.cloudant.design.IndexSearch
import com.wieck.cloudant.CloudantProtocol

case class IndexSearchOptions[K: JsonFormat](query: Option[String], includeDocs: Option[Boolean], sort: IndexSearch.SortParameter,
  limit: Option[Int], bookmark: Option[String], stale: Option[Boolean]) {

  import CloudantProtocol._

  def asQueryParams: Map[String, String] = {
    Map(
      "q" -> query,
      "include_docs" -> includeDocs,
      "sort" -> sortParamAsQueryParam,
      "limit" -> limit,
      "bookmark" -> bookmark.map(identity)
    ) collect { case(a, Some(b)) => (a, b.toString()) }
  }

  // Convert sort parameter to an Option[String] based on the type (None, String, or List).
  private def sortParamAsQueryParam: Option[String] =
    sort match {
      case IndexSearch.NoSortParameter =>
        None
      case IndexSearch.StringSortParameter(value) =>
        Some(quoteParam(value))
      case IndexSearch.ListSortParameter(values: List[String]) =>
        Some("[" + values.map(quoteParam).mkString(", ") + "]")
    }

  private def quoteParam(param: String): String =
    "\"" + param + "\""

}

case class IndexSearchBuilder[K: JsonFormat](_query: Option[String], _includeDocs: Option[Boolean], _sort: IndexSearch.SortParameter,
  _limit: Option[Int], _bookmark: Option[String], _stale: Option[Boolean]) {

  def options: IndexSearchOptions[K] = IndexSearchOptions[K](_query, _includeDocs, _sort, _limit, _bookmark, _stale)

  def query(s: String) = copy[K](_query = Some(s))

  def includeDocs = copy[K](_includeDocs = Some(true))

  def limit(n: Int) = copy[K](_limit = Some(n))

  def sort(s: String) = copy[K](_sort = IndexSearch.StringSortParameter(s))

  def sort(l: List[String]) = copy[K](_sort = IndexSearch.ListSortParameter(l))

  def sort(o: Option[String]) = copy[K](_sort =
    o match {
      case Some(s) => IndexSearch.StringSortParameter(s)
      case None => IndexSearch.NoSortParameter
    }
  )

  def bookmark(s: String) = copy[K](_bookmark = Some(s))

  def bookmark(s: Option[String]) = copy[K](_bookmark = s)
}

object IndexSearchBuilder {

  def apply[K: JsonFormat] =
    new IndexSearchBuilder[K](_query = None, _includeDocs = None,
      _sort = IndexSearch.NoSortParameter, _limit = None, _bookmark = None,
      _stale = None)

}

package com.wieck.cloudant.query

import spray.json.JsonFormat

case class PaginationBuilder[K: JsonFormat](_direction: PaginationDirection.PaginationDirection, _pageSize: Int,
  _key: Option[K]) {

  def options: PaginationOptions[K] = PaginationOptions(_direction, _pageSize, _key)

  def key[K: JsonFormat](k: K) = copy[K](_key = Some(k))

  def key[K: JsonFormat](k: Option[K]) = copy[K](_key = k)

  def pageSize(pageSize: Int) = copy[K](_pageSize = pageSize)

  def direction(d: PaginationDirection.PaginationDirection) = copy(_direction = d)

  def forward = copy(_direction = PaginationDirection.Forward)

  def reverse = copy(_direction = PaginationDirection.Reverse)

}

object PaginationBuilder {

  def apply[K: JsonFormat] =
    new PaginationBuilder[K](_direction = PaginationDirection.Forward, _pageSize = 20, _key = None)

}
package com.wieck.cloudant.query

import spray.json.JsonFormat
import com.wieck.cloudant._

case class PaginatedViewQuery[K1: JsonFormat, V: JsonFormat, K2: JsonFormat, K3: JsonFormat](database: Database,
  designDocumentId: String, viewName: String, queryBuilder: ViewQueryBuilder[K1, K2, K3],
  paginationBuilder: PaginationBuilder[K1]) {

  def execute = database.queryViewWithPagination[K1, V](designDocumentId, viewName, paginationBuilder._pageSize,
    paginationBuilder._direction, paginationBuilder._key,
    queryBuilder.options.asQueryParams ++ paginationBuilder.options.asQueryParams)

  def pageSize(pageSize: Int) = copy[K1, V, K2, K3](paginationBuilder = paginationBuilder.pageSize(pageSize))

  def key(k: K1) = copy[K1, V, K2, K3](paginationBuilder = paginationBuilder.key(k))

  def key(k: Option[K1]) = copy[K1, V, K2, K3](paginationBuilder = paginationBuilder.key(k))

  def direction(d: PaginationDirection.PaginationDirection) =
    copy[K1, V, K2, K3](paginationBuilder = paginationBuilder.direction(d))

  def includeDocs = copy[K1, V, K2, K3](queryBuilder = queryBuilder.includeDocs)

}

object PaginatedViewQuery {

  def apply[K: JsonFormat, V: JsonFormat, K2: JsonFormat, K3: JsonFormat](database: Database, designDocumentId: String,
    viewName: String): PaginatedViewQuery[K, V, K2, K3] = {

    new PaginatedViewQuery[K, V, K2, K3](database, designDocumentId, viewName, ViewQueryBuilder[K, K2, K3],
      PaginationBuilder[K])
  }

  def apply[K: JsonFormat, V: JsonFormat, K2: JsonFormat, K3: JsonFormat](database: Database, designDocumentId: String,
    viewName: String, queryBuilder: ViewQueryBuilder[K, K2, K3]): PaginatedViewQuery[K, V, K2, K3] = {

    new PaginatedViewQuery[K, V, K2, K3](database, designDocumentId, viewName, queryBuilder, PaginationBuilder[K])
  }

}
package com.wieck.cloudant.query

import spray.json._
import com.wieck.cloudant.CloudantProtocol

case class ViewQueryOptions[K1: JsonFormat, K2: JsonFormat, K3: JsonFormat](key: Option[K1], startKey: Option[K2],
  endKey: Option[K3], inclusiveEnd: Option[Boolean], includeDocs: Option[Boolean], includeDesign: Option[Boolean],
  limit: Option[Int], group: Option[Boolean], descending: Option[Boolean]) extends CloudantProtocol {

  def asQueryParams: Map[String, String] = {
    Map(
      "key" -> key.map(_.toJson),
      "inclusive_end" -> inclusiveEnd,
      "include_docs" -> includeDocs,
      "include_design" -> includeDesign,
      "limit" -> limit,
      "startkey" -> startKey.map(_.toJson),
      "endkey" -> endKey.map(_.toJson),
      "group" -> group.map(_.toJson),
      "descending" -> descending.map(_.toJson)
    ) collect { case(a, Some(b)) => (a, b.toString()) }
  }

}

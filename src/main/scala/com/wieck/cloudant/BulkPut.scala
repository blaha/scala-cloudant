package com.wieck.cloudant

case class BulkPut[A](docs: Seq[Document[A]])
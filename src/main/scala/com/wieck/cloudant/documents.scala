package com.wieck.cloudant

sealed trait Document[A] {

  def id: String

  def data: A

  def revise(rev: String): RevisedDocument[A] = RevisedDocument(id, data, rev)

  def update(f: A => A): Document[A]

}

case class NewDocument[A](id: String, data: A) extends Document[A] {

  def update(f: A => A): NewDocument[A] = copy(data = f(data))

}

case class RevisedDocument[A](id: String, data: A, rev: String) extends Document[A] {

  override def revise(rev: String): RevisedDocument[A] = copy(rev = rev)

  def update(f: A => A): RevisedDocument[A] = copy(data = f(data))

}
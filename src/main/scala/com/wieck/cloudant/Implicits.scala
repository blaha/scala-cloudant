package com.wieck.cloudant

import design._
import scala.language.implicitConversions
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object Implicits {

  implicit def stringToMapReduce(string: String) = MapReduce(map = string)

  implicit def tuple2ToUpdateFunction(pair: Tuple2[String, String]) = UpdateFunction(pair._1, pair._2)
  implicit def updateFunctionToTuple2(f: UpdateFunction) = (f.name, f.updateFunction)

  implicit def basicViewToSeqViewDef(view: BasicView) = Seq(view)

  implicit def basicViewToTuple2(view: BasicView) = Tuple2(view.name, view.mapReduce)

  implicit def tuple2ToViewDef(tuple2: Tuple2[String, MapReduce]): ViewDefinition = BasicView(tuple2._1, tuple2._2)

  implicit def tuple2ToSeqViewDef(tuple2: Tuple2[String, MapReduce]): Seq[ViewDefinition] =
    Seq(BasicView(tuple2._1, tuple2._2))

  implicit def seqTuple2ToSeqViewDef(seq: Seq[Tuple2[String, MapReduce]]): Seq[ViewDefinition] =
    seq.map(tuple2ToViewDef)

  implicit def mapToViewDefSeq(map: Map[String, MapReduce]): Seq[ViewDefinition] =
    map.map { case (k, v) => BasicView(k, v) }.toVector

  implicit def mapToIndexDefSeq(map: Map[String, IndexFunction]): Seq[IndexDefinition] =
    map.map { case (k, v) => BasicIndex(k, v) }.toVector

  implicit def mapToUpdateFunctionSeq(map: Map[String, String]): Seq[UpdateFunction] =
    map.map { case (k, v) => UpdateFunction(k, v) }.toVector

}

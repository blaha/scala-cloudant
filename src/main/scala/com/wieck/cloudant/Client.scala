package com.wieck.cloudant

import com.typesafe.scalalogging.StrictLogging
import design._
import scala.util.{Try, Success, Failure}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import dispatch._
import spray.json._
import com.wieck.cloudant.exceptions._
import com.wieck.cloudant.responses._

case class ServerError(statusCode: Int, message: String) extends Exception {
  override def toString = s"server error response: $statusCode $message"
}


case class Client(hostname: String, username: String, password: String) extends StrictLogging {

  import CloudantProtocol._

  /**
   * Create a new database.
   * @param dbName the database name
   * @return a future [[Database]]
   */
  def createDatabase(dbName: String): Future[Database] =
    apply[OkResponse](_.PUT / dbName) map {
      case Success(_) => Database(this, dbName)
      case Failure(ServerError(412, _)) => throw DatabaseAlreadyExistsException(dbName)
      case Failure(e) => throw e
    }

  /**
   * Get an existing database.
   * @param dbName the database name
   * @return a future [[Database]]
   */
  def getDatabase(dbName: String): Future[Database] =
    databaseInfo(dbName) map (_ => Database(this, dbName))

  def getOrCreateDb(dbName: String): Future[Database] = {
    getDatabase(dbName) recoverWith {
      case e => logger.info(s"creating new database [$dbName]", e); createDatabase(dbName)
    }
  }

  /**
   * Delete an existing database.
   * @param dbName the database name
   * @return a future Unit
   */
  def deleteDatabase(dbName: String): Future[Unit] =
    apply[OkResponse](_.DELETE / dbName) map {
      case Success(_) => ()
      case Failure(ServerError(404, _)) => throw DatabaseNotFoundException(dbName)
      case Failure(e) => throw e
    }

  /**
   * Get the latest revision of the document.
   * @param dbName the database name.
   * @param id the ID of the document.
   * @tparam A the type to deserialize the document as.
   * @return a future [[RevisedDocument]]
   */
  def getDocument[A: RootJsonFormat](dbName: String, id: String): Future[RevisedDocument[A]] =
    apply[RevisedDocument[A]](_ / dbName / id) map {
      case Success(s) => s
      case Failure(ServerError(404, _)) => throw DocumentNotFoundException(dbName, id)
      case Failure(e) => throw e
    }

  def putDocument[A: RootJsonFormat](dbName: String, document: Document[A], quorum: Option[Int] = None)
    : Future[RevisedDocument[A]] = {

    val params = Map("w" -> quorum) collect { case(a, Some(b)) => (a, b.toString()) }
    apply[DocumentResponse](_.PUT.setContentType("application/json", "UTF-8") / dbName / document.id <<? params << document.toJson.compactPrint) map {
      case Success(response) => document.revise(response.rev)
      case Failure(ServerError(409, _)) => throw DocumentConflictException(dbName, document.id)
      case Failure(e) => throw e
    }
  }

  def deleteDocument(dbName: String, id: String, rev: String, quorum: Option[Int] = None): Future[Unit] = {
    val params = Map("w" -> quorum, "rev" -> Some(rev)) collect { case(a, Some(b)) => (a, b.toString()) }

    apply[OkResponse](_.DELETE / dbName / id <<? params) map {
      case Success(_) => ()
      case Failure(ServerError(409, _)) => throw DocumentConflictException(dbName, id)
      case Failure(ServerError(404, _)) => throw DocumentNotFoundException(dbName, id)
      case Failure(e) => throw e
    }
  }

  def allDocuments(database: String, params: Map[String, String] = Map()): Future[AllDocumentsResponse] =
    apply[AllDocumentsResponse](_ / database / "_all_docs" <<? params).map(_.get)

  def bulkPut[A: RootJsonFormat](dbName: String, documents: Seq[Document[A]], quorum: Option[Int] = None)
    : Future[Seq[RevisedDocument[A]]] = {

    val bulk = BulkPut(documents).toJson.compactPrint
    val params = Map("w" -> quorum) collect { case(a, Some(b)) => (a, b.toString()) }
    apply[Seq[BulkDocumentResponse]](
      _.POST.setContentType("application/json", "UTF-8") / dbName / "_bulk_docs" << bulk <<? params
    ) map {
      case Success(response) => response.zip(documents) map { case (cdr, doc) => doc.revise(cdr.rev) }
      case Failure(e) => throw e
    }
  }

  def getDesignDocument(dbName: String, id: String): Future[RevisedDocument[DesignDocument]] =
    getDocument[DesignDocument](dbName, s"_design/$id")

  def createDesignDocument(dbName: String, id: String, designDocument: DesignDocument) =
    putDocument(dbName, NewDocument(s"_design/$id", designDocument), Some(3))

  def deleteDesignDocument(dbName: String, id: String, rev: String) =
    deleteDocument(dbName, s"_design/$id", rev, Some(3))

  def queryView[K: JsonFormat, V: JsonFormat](dbName: String, designDocumentId: String, viewName: String,
                                              params: Map[String, String]): Future[ViewResponse[K, V]] = {

    apply[ViewResponse[K, V]](_ / dbName / "_design" / designDocumentId / "_view" / viewName <<? params).map(_.get)
  }

  def reduceView[K: JsonFormat, V: JsonFormat](dbName: String, designDocumentId: String, viewName: String,
                                              params: Map[String, String]): Future[ReduceResponse[K, V]] = {

    apply[ReduceResponse[K, V]](_ / dbName / "_design" / designDocumentId / "_view" / viewName <<? params).map(_.get)
  }

  def runDocumentUpdate[Data: RootJsonFormat](dbName: String, designDocumentId: String, documentId: String, updateFunc: String, data: Data): Future[Try[String]] = {
    applyRaw(_.PUT.setContentType("application/json", "UTF-8") / dbName / "_design" / designDocumentId / "_update" / updateFunc / documentId << data.toString)
  }

  def searchIndex[K: JsonFormat, V: JsonFormat](dbName: String, designDocumentId: String, indexName: String,
    params: Map[String, String]): Future[SearchResponse[K, V]] = {

    apply[SearchResponse[K, V]](_ / dbName / "_design" / designDocumentId / "_search" / indexName <<? params).map(_.get)
  }

  def databaseInfo(database: String): Future[DatabaseInfo] =
    apply[DatabaseInfo](_ / database) map {
      case Success(info) => info
      case Failure(ServerError(404, _)) => throw new DatabaseNotFoundException(database)
      case Failure(e) => throw e
    }

  def serverInfo: Future[ServerInfo] = apply[ServerInfo](identity).map(_.get)

  def serverVersion: Future[String] = serverInfo map (_.version)

  /**
   * Makes sure all uncommitted changes are written and synchronized to the disk.
   * @param dbName the database name
   * @return a future unit
   */
  def ensureFullCommit(dbName: String): Future[Unit] =
    apply[OkResponse](_.POST.setContentType("application/json", "UTF-8") / dbName / "_ensure_full_commit") map (_ => ())


  def apply[T: RootJsonFormat](req: Req => Req): Future[Try[T]] = {
    applyRaw(req) map {
      case Success(body) => Try(body.parseJson.convertTo[T])
      case Failure(err) => Failure(err)
    }
  }


  def applyRaw(req: Req => Req): Future[Try[String]] = {
    val insecureRequest = req(host(hostname))
    if (logger.underlying.isTraceEnabled) {
      val request = insecureRequest.toRequest
      logger.trace(s"request: $request\n${request.getStringData}")
    }

    val request = req(host(hostname)).secure.as_!(username, password).toRequest
    val requestStartNanos = System.nanoTime()
    Http(request, new FunctionHandler(identity)).either map {
      case Right(response) => 
        val duration = (System.nanoTime() - requestStartNanos) / 1000000
        val code = response.getStatusCode
        logResponse(request.getMethod, insecureRequest.url, code, duration, response.getResponseBody("UTF-8"))

        if (code / 100 == 2) Success(response.getResponseBody("UTF-8"))
        else Failure(ServerError(code, response.getResponseBody("UTF-8")))

      case Left(e) => 
        logger.error(s"Unexpected exception: $e")
        Failure(e)
    }
  }


  protected def logResponse(method: String, url: String, statusCode: Int, duration: Long, body: String): Unit = {
    if (logger.underlying.isTraceEnabled) logger.trace(s"$statusCode ${duration}ms $method $url $body")
    else logger.info(s"$statusCode ${duration}ms $method $url")
  }
}

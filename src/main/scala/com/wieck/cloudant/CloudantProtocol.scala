package com.wieck.cloudant

import design._
import spray.json._
import com.wieck.cloudant.responses._
import com.wieck.cloudant.design.indexes._

trait CloudantProtocol extends DefaultJsonProtocol {

  import Implicits._

  implicit val nullFormat: RootJsonFormat[Null] = new RootJsonFormat[Null] {

    override def read(js: JsValue): Null = null

    override def write(n: Null): JsNull.type = JsNull

  }

  implicit val okResponseFormat = jsonFormat1(OkResponse)

  implicit val serverInfoFormat = jsonFormat2(ServerInfo)

  implicit val databaseInfoOtherFormat = jsonFormat1(DatabaseInfoOther)

  implicit val databaseInfoFormat = jsonFormat10(DatabaseInfo)

  implicit val documentResponseFormat = jsonFormat3(DocumentResponse)

  class NewDocumentFormat[A: RootJsonFormat] extends RootJsonFormat[NewDocument[A]] {

    def read(value: JsValue) =
      value match {
        case o: JsObject => {
          val id = o.fields("_id").convertTo[String]
          val data = o.convertTo[A]
          NewDocument(id, data)
        }
        case _ => deserializationError("DocumentFormat expects a JsObject.")
      }

    def write(newDocument: NewDocument[A]) =
      JsObject(
        Map(
          "_id" -> newDocument.id.toJson
        ) ++ newDocument.data.toJson.asJsObject.fields
      )

  }

  class RevisedDocumentFormat[A: RootJsonFormat] extends RootJsonFormat[RevisedDocument[A]] {

    def read(value: JsValue) =
      value match {
        case o: JsObject => {
          val id = o.fields("_id").convertTo[String]
          val data = o.convertTo[A]
          val rev = o.fields("_rev").convertTo[String]
          RevisedDocument(id, data, rev)
        }
        case _ => deserializationError("DocumentFormat expects a JsObject.")
      }

    def write(revisedDocument: RevisedDocument[A]) =
      JsObject(
        Map(
          "_id" -> revisedDocument.id.toJson,
          "_rev" -> revisedDocument.rev.toJson
        ) ++ revisedDocument.data.toJson.asJsObject.fields
      )
  }

  class DocumentFormat[A: RootJsonFormat] extends RootJsonFormat[Document[A]] {

    def read(value: JsValue) = value.convertTo[RevisedDocument[A]]

    def write(document: Document[A]) =
      document match {
        case newDocument: NewDocument[A] => newDocument.toJson
        case revisedDocument: RevisedDocument[A] => revisedDocument.toJson
      }

  }

  implicit def documentFormat[A: RootJsonFormat]: RootJsonFormat[Document[A]] = new DocumentFormat[A]

  implicit def newDocumentFormat[A: RootJsonFormat]: RootJsonFormat[NewDocument[A]] = new NewDocumentFormat[A]

  implicit def revisedDocumentFormat[A: RootJsonFormat]: RootJsonFormat[RevisedDocument[A]] = new RevisedDocumentFormat[A]

  implicit def key1Format[A: JsonFormat] = new JsonFormat[Key1[A]] {
    def write(k: Key1[A]) = k._1.toJson
    def read(value: JsValue) = value match {
      case JsArray(a +: Vector()) => Key1(a.convertTo[A])
      case x => deserializationError("Expected Key1 as JsArray, but got " + x)
    }
  }

  implicit def key2Format[A: JsonFormat, B: JsonFormat] = new JsonFormat[Key2[A, B]] {
    def write(k: Key2[A, B]) = JsArray(k._1.toJson, k._2.toJson)
    def read(value: JsValue) = value match {
      case JsArray(a +: b +: Vector()) => Key2(a.convertTo[A], b.convertTo[B])
      case x => deserializationError("Expected Key2 as JsArray, but got " + x)
    }
  }

  implicit def key3Format[A: JsonFormat, B: JsonFormat, C: JsonFormat] = new JsonFormat[Key3[A, B, C]] {
    def write(k: Key3[A, B, C]) = JsArray(k._1.toJson, k._2.toJson, k._3.toJson)
    def read(value: JsValue) = value match {
      case JsArray(a +: b +: c +: Vector()) => Key3(a.convertTo[A], b.convertTo[B], c.convertTo[C])
      case x => deserializationError("Expected Key3 as JsArray, but got " + x)
    }
  }

  implicit val mapReduceFormat = jsonFormat2(MapReduce)


  class IndexFunctionFormat extends RootJsonFormat[IndexFunction] with DefaultJsonProtocol {

    class AnalyzerFormat extends JsonFormat[Analyzer] {

      def read(value: JsValue) = {
        value match {
          case JsString(Analyzer(analyzer)) => analyzer
          case perfield:JsObject => {
            perfield.getFields("name", "default", "fields") match {
              case Seq(JsString("perfield"), JsString(Analyzer(default)), fields:JsObject) => {
                PerFieldAnalyzer(
                  default,
                  fields.fields.mapValues { case JsString(Analyzer(analyzer)) => analyzer })
              }
              case _ => throw new DeserializationException(s"Malformed PerFieldAnalyzer: $perfield")
            }

          }
          case _ => throw new DeserializationException(s"Unknown Analyzer: $value")
        }
      }

      def write(value: Analyzer) = {
        value match {
          case pfa: PerFieldAnalyzer => JsObject(
            Map(
              "name" -> pfa.name.toJson,
              "default" -> pfa.default.toJson,
              "fields" -> JsObject(pfa.fields.map(f => f._1 -> f._2.toJson))
            )
          )
          case _ => JsString(value.name)
        }
      }

    }

    implicit val analyzerFormat = new AnalyzerFormat

    val format = jsonFormat2(IndexFunction)

    def read(value: JsValue) = format.read(value)

    def write(indexFunction: IndexFunction) = format.write(indexFunction)

  }

  implicit val indexFunctionFormat = new IndexFunctionFormat


  class DesignDocumentFormat extends RootJsonFormat[DesignDocument] with DefaultJsonProtocol {

    override def read(value: JsValue) = {
      value.asJsObject.getFields("views", "indexes", "updates", "language", "timestamp") match {
        case Seq(views, indexes, updates, JsString(language), JsNumber(timestamp)) => {
          DesignDocument(
            views.convertTo[Map[String, MapReduce]],
            indexes.convertTo[Map[String, IndexFunction]],
            updates.convertTo[Map[String, String]],
            language,
            timestamp.toLong)
        }
        case _ => throw new DeserializationException(s"DesignDocument expected: \n${value.prettyPrint}")
      }
    }

    override def write(designDocument: DesignDocument) = {
      JsObject(
        "language" -> designDocument.language.toJson,
        "views" -> designDocument.views.map(view => view.name -> view.mapReduce).toMap.toJson,
        "indexes" -> designDocument.indexes.map(index => index.name -> index.indexFunction).toMap.toJson,
        "updates" -> designDocument.updateFunctions.map(f => f.name -> f.updateFunction).toMap.toJson,
        "timestamp" -> designDocument.timestamp.toJson
      )
    }

  }

  implicit val designDocumentFormat = new DesignDocumentFormat

  implicit def viewRowFormat[K: JsonFormat, V: JsonFormat] = jsonFormat3(ViewRow[K,V])

  implicit def viewResponseFormat[K: JsonFormat, V: JsonFormat] = jsonFormat3(ViewResponse[K,V])

  implicit def reduceResponseFormat[K: JsonFormat, V: JsonFormat] = jsonFormat1(ReduceResponse[K,V])

  implicit val keyBoundsMinFormat = new JsonFormat[KeyBounds.Min.type] {
    override def read(js: JsValue) = KeyBounds.Min
    override def write(bounds: KeyBounds.Min.type) = JsNull
  }

  implicit val keyBoundsMaxFormat = new JsonFormat[KeyBounds.Max.type] {
    override def read(js: JsValue) = KeyBounds.Max
    override def write(bounds: KeyBounds.Max.type) = JsObject()
  }

  implicit def bulkPutFormat[A: RootJsonFormat] = jsonFormat1(BulkPut[A])

  implicit val bulkDocumentResponseFormat = jsonFormat2(BulkDocumentResponse)

  implicit val allDocumentsValueFormat = jsonFormat1(AllDocumentsValue)

  implicit val allDocumentsRowFormat = jsonFormat3(AllDocumentsRow)

  implicit val allDocumentsResponseFormat = jsonFormat3(AllDocumentsResponse)

  implicit def searchRowFormat[K: JsonFormat, V: JsonFormat] = jsonFormat4(SearchRow[K,V])

  implicit def searchResponseFormat[K: JsonFormat, V: JsonFormat] = jsonFormat3(SearchResponse[K,V])

}

object CloudantProtocol extends CloudantProtocol

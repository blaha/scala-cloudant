package com.wieck.cloudant.exceptions

case class DocumentConflictException(dbName: String, documentId: String)
  extends Exception(s"Document conflict in database $dbName for document $documentId")

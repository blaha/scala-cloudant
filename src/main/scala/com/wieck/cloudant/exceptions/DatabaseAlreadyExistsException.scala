package com.wieck.cloudant.exceptions

case class DatabaseAlreadyExistsException(name: String) extends Exception(s"Database already exists: $name")

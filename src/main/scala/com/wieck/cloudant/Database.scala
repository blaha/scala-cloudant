package com.wieck.cloudant

import design._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Try
import spray.json._
import com.wieck.cloudant.responses._
import com.wieck.cloudant.exceptions._
import com.wieck.cloudant.query.{PaginatedViewResponse, PaginationDirection}

trait Database {

  val client: Client
  val name: String

  def putDocument[A: RootJsonFormat](document: Document[A], quorum: Option[Int] = None) =
    client.putDocument[A](this.name, document, quorum)

  def getDocument[A: RootJsonFormat](id: String) = client.getDocument[A](this.name, id)

  def getDocumentOption[A: RootJsonFormat](id: String) = {
    client.getDocument(this.name, id) map { doc => Some(doc) } recover { case e: DocumentNotFoundException => None }
  }

  def deleteDocument(id: String, rev: String, quorum: Option[Int] = None): Future[Unit] = client.deleteDocument(this.name, id, rev, quorum)

  def allDocuments(params: Map[String, String] = Map.empty) = client.allDocuments(this.name, params)

  def bulkPut[A: RootJsonFormat](docs: Traversable[Document[A]], quorum: Option[Int] = None): Future[Seq[RevisedDocument[A]]] =
    client.bulkPut[A](this.name, docs.toSeq, quorum)

  def createDesignDocument(id: String, designDocument: DesignDocument): Future[RevisedDocument[DesignDocument]] =
    client.createDesignDocument(this.name, id, designDocument)

  def createOrUpdateDesignDocument(id: String, designDocument: DesignDocument): Future[RevisedDocument[DesignDocument]] = {
    client.getDesignDocument(this.name, id) map {
      doc => Some(doc)
    } recover {
      case _: DocumentNotFoundException => None
    } flatMap {
      case None => createDesignDocument(id, designDocument)
      case Some(doc) => {
        if(designDocument contentEquals doc.data) {
          Future.successful(doc)
        } else {
          client.deleteDesignDocument(this.name, id, doc.rev) flatMap { _ =>
            createDesignDocument(id, designDocument)
          }
        }
      }
    }
  }

  def queryView[K: JsonFormat, V: JsonFormat](designDocumentId: String, viewName: String)
    : Future[ViewResponse[K, V]] = {

    queryView[K, V](designDocumentId, viewName, Map.empty[String, String])
  }

  def queryView[K: JsonFormat, V: JsonFormat](designDocumentId: String, viewName: String,
                                              params: Map[String, String]): Future[ViewResponse[K, V]] = {

    client.queryView[K, V](this.name, designDocumentId, viewName, params)
  }

  def reduceView[K: JsonFormat, V: JsonFormat](designDocumentId: String, viewName: String,
                                              params: Map[String, String]): Future[ReduceResponse[K, V]] = {

    client.reduceView[K, V](this.name, designDocumentId, viewName, params)
  }

  def queryViewWithPagination[K: JsonFormat, V: JsonFormat](designDocumentId: String, viewName: String, pageSize: Int,
    direction: PaginationDirection.PaginationDirection, key: Option[K], params: Map[String, String] = Map())
    : Future[PaginatedViewResponse[K, V]] = {

    client.queryView[K, V](this.name, designDocumentId, viewName, params) map { response =>
      PaginatedViewResponse[K, V](response, pageSize, direction, key)
    }
  }

  def runDocumentUpdate[Data: RootJsonFormat](designDocumentId: String, documentId: String, updateFunc: String, data: Data): Future[Try[String]] = {
    client.runDocumentUpdate[Data](this.name, designDocumentId, documentId, updateFunc, data)
  }

  def searchIndex[K: JsonFormat, V: JsonFormat](designDocId: String, indexName: String, params: Map[String, String])
    : Future[SearchResponse[K, V]] = {

    client.searchIndex[K, V](this.name, designDocId, indexName, params)
  }

  def delete() = client.deleteDatabase(this.name)

  def ensureFullCommit(): Future[Unit] = client.ensureFullCommit(this.name)

}

object Database {

  def apply(client: Client, name: String) = {
    val (_client, _name) = (client, name)

    new Object with Database {
      val client = _client
      val name = _name
    }
  }

}

package com.wieck.cloudant.design.indexes

case class PerFieldAnalyzer(default: Analyzer = analyzers.Standard, fields: Map[String, Analyzer]) extends Analyzer {

  val name = "perfield"

}
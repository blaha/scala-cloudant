package com.wieck.cloudant.design

trait ViewDefinition {

  def name: String

  def mapReduce: MapReduce

}
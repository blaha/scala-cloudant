package com.wieck.cloudant.design

trait IndexDefinition {

  def name: String

  def indexFunction: IndexFunction

}

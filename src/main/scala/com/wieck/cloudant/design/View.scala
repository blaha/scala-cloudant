package com.wieck.cloudant.design

import spray.json._
import com.wieck.cloudant.{CloudantProtocol, Database}
import com.wieck.cloudant.query.{ViewQueryBuilder, ViewQuery}

class View[K: JsonFormat, V: JsonFormat](database: Database, designDocumentId: String, name: String) {

  import CloudantProtocol._

  def query = ViewQuery[K, V, Null, Null](database, designDocumentId, name, ViewQueryBuilder[K, Null, Null])

  def page = query.page

}

object View {

  def apply[K: JsonFormat, V: JsonFormat](database: Database, designDocumentId: String, name: String) =
    new View[K, V](database, designDocumentId, name)

}
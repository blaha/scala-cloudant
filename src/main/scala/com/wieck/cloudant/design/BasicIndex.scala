package com.wieck.cloudant
package design

case class BasicIndex(name: String, indexFunction: IndexFunction) extends IndexDefinition

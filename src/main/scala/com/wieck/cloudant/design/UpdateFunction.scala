package com.wieck.cloudant.design

trait UpdateDefinition {
  val name: String
  val updateFunction: String
}

case class UpdateFunction(name: String, updateFunction: String) extends UpdateDefinition

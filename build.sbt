name := "scala-cloudant"

organization := "com.wieck"

licenses := Seq("BSD-style" -> url("http://www.opensource.org/licenses/bsd-license.php"))

homepage := Some(url("http://bitbucket.org/wieckmedia/scala-cloudant"))

version := "1.2.0"

scalaVersion := "2.11.7"

scalacOptions in ThisBuild ++= Seq(
  "-language:_",
  "-feature",
  "-unchecked",
  "-deprecation")

parallelExecution in Test := false

resolvers += "Wieck Nexus" at "https://nexus.wieck.com/content/groups/public"

publishTo := {
  val nexus = "https://nexus.wieck.com/content/repositories/"
  if (isSnapshot.value)
    Some("Wieck Snapshots" at nexus + "snapshots")
  else
    Some("Wieck Releases"  at nexus + "releases")
}

val testDependencies = Seq(
  "org.scalatest"            %% "scalatest"           % "2.2.4"  % "test",
  "com.typesafe"              % "config"              % "1.2.1"  % "test")

libraryDependencies ++= testDependencies ++ Seq(
  "net.databinder.dispatch"  %% "dispatch-core"       % "0.11.3",
  "io.spray"                 %% "spray-json"          % "1.3.2",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0")
